#include <stdio.h>
#include <stdlib.h>
/*
 * This is a c program that does something cool.
 */


int main(int argc, char *argv[]) {
	printf("===========\n"); //This is a single-line comment.
	printf("Hello world\n");
	printf("===========\n\n");

	if (argc != 4) {
		printf("Usage: ./RedBluePPM OUTfileName numrow numcol\n");
		exit(1);
	}

	if ((numRows = atoi(argv[2])) <= 0) {
		printf("Error: numRows needs to be positive");
		exit(1);
	}

	return 0;
}
