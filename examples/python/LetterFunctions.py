""" Library of functions that can be called in letter writing applications."""

import sys
import os
import math
from string import whitespace

class Address:
	def __init__(self, Last, First, AddLine1, AddLine2):
		self.Last = Last
		self.First = First
		self.AddLine1 = AddLine1
		self.AddLine2 = AddLine2

	def dumpAddresses(self):
		print "----------------------"
		print "|Address Info:"
		print "|", self.First, " ", self.Last
		print "|", self.AddLine1
                print "|", self.AddLine2
		print "----------------------"

class DocumentText:
	def __init__ (self, address):
		self.address = address

		self.header = ("\\documentclass[12pt]{article} \n"
			    +  "\\usepackage{geometry} \n"
                            +  "\\geometry{hmargin={1in, 1in}, vmargin = {2in, 1in}} \n"
                            +  "\\begin{document} \n"
                            +  "\\thispagestyle{empty} \n\n")

		self.myAddress = "My Address \n\n \\vskip.5in \n\n"
		self.toAddress = (self.address.First + " " + self.address.Last + "\n\n"
			       +  self.address.AddLine1 + "\n\n"
			       +  self.address.AddLine2 + "\n\n")

		self.date = "\\vskip.5in \n \today \n\n \\vskip.5in \n\n"

		self.greeting = "Dear " + self.address.First + " " + self.address.Last + ". \n\n \\vskip.5in"

		self.body = ("THE GEOGRAPY I STAND COMPARES YOU SUPERIOR"
			  +  "\n\n \vskip.5in \n\n \\hskip3.5in Hello there, \n\n"
			  +  "\\hskip3.5in Ratio Tile \n\n")

		self.footer ="\\end{document}\n"

	def writeLetter(self):
		LetterOutName = str(self.address.Last.lstrip(' ')) + ".tex"
		LetterFile = open(LetterOutName, "w")
		LetterFile.write(self.header)
		LetterFile.write(self.myAddress)
		LetterFile.write(self.toAddress)
		LetterFile.write(self.date)
		LetterFile.write(self.greeting)
		LetterFile.write(self.body)
		LetterFile.write(self.footer)
