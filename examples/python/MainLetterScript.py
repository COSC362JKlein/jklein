""" This is a script to do a mail merge to make custom letters to mythical beings.
    To Run:
	[user@machine /directory]$ python MainLetterScript.py NamesandAddresses.csv """

from LetterFunctions import *

# ==================================================================================
# READ COMMAND LINE ARGS
# ==================================================================================
if len(sys.argv) != 2:
	print "To Run: python MainLetterScript.py NamesandAddresses.csv"
else:
	print "Using addresses from file:", str(sys.argv[1])
	dataFile = str(sys.argv[1])

# ==================================================================================
# OPEN FILES
# ==================================================================================
AddressData = open(dataFile, "r")

# ==================================================================================
# READ DATA FROM FILE LINE BY LINE
# ==================================================================================
print "Reading from file: "  + dataFile

AddressList = []
for line in AddressData.readlines():
	Last, First, Address1, Address2 = map(str, line.split(","))
	CurrentAddress = Address(Last, First, Address1, Address2)
	AddressList.append(CurrentAddress)
	# CurrentAddress.dumpAddresses()

# ==================================================================================
# WRITE OUT FORM LETTERS
# ==================================================================================
for i in range(len(AddressList)):
	Letter = DocumentText(AddressList[i])
	Letter.writeLetter()
