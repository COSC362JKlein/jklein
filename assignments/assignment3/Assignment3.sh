#!/bin/bash

for file in *.tex
do
	pdflatex $file
done

rm -rf *.log *.aux

for file in *.pdf *.tex
do
	wc -w $file >> LatexCompileReport.txt
done
