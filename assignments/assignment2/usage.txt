top - 12:38:28 up 7 days,  9:10, 24 users,  load average: 0.40, 0.48, 0.58
Tasks: 215 total,   1 running, 178 sleeping,   1 stopped,   0 zombie
%Cpu(s): 21.0 us,  2.6 sy,  0.0 ni, 75.9 id,  0.4 wa,  0.0 hi,  0.0 si,  0.0 st
KiB Mem :  4918980 total,  1001472 free,  2891584 used,  1025924 buff/cache
KiB Swap:  2069500 total,  2066428 free,     3072 used.  1898444 avail Mem 

  PID USER      PR  NI    VIRT    RES    SHR S %CPU %MEM     TIME+ COMMAND
11905 klein     20   0   48868   3644   3052 R  5.9  0.1   0:00.02 top
 4361 klein     20   0   94924   3376   2436 S  0.0  0.1   0:00.04 sshd
 4362 klein     20   0   29680   5184   3292 S  0.0  0.1   0:00.14 bash
 9107 klein     20   0   16724   2376   2180 S  0.0  0.0   0:00.00 less
63927 klein     20   0   45276   4564   3860 S  0.0  0.1   0:00.07 systemd
63928 klein     20   0  145380   2052      0 S  0.0  0.0   0:00.00 (sd-pam)
63955 klein     20   0   95012   4504   3456 S  0.0  0.1   0:00.45 sshd
63956 klein     20   0   29732   5400   3436 S  0.0  0.1   0:00.31 bash
