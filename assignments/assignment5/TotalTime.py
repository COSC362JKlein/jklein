import glob, os

# Total report file created and given header details
report = "report.tex"
report_write = open(report, "w")
report_write.write("\documentclass[12pt]{article}\n")
report_write.write("\\begin{document}\n")
report_write.write("\\begin{center}\n")
report_write.write("{\Large \\bf\n")
report_write.write("Time Report by Person}\n")
report_write.write("\end{center}\n")

# Each individual log opened (txt format required)
for file in glob.glob("*.txt"):
	indiv_file = open(file, "r")
	
	# Array of days to trim off of data
	week = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]

	# Variables that hold total time for each person
	total_hours = 0
	total_mins = 0

	# Each ine of file checked. If the line has time data,
	# the hours and minutes are added to the running totals.
	# Else the person's name is written to a report.
	for line in indiv_file:
		if any(day in line for day in week):

			first, sep, extra = line.partition("hr")
			day, space, hour = first.partition(" ")
			total_hours += int(hour)

			first, sep, extra = line.partition("hr ")
			min = extra.split("min")[0]
			total_mins += int(min)

		else:
			report_write.write(line + "\n")

	# Total hours and mintues converted/written to report and file reader closed
	revised_tm = total_mins % 60
	total_hours = total_hours + (total_mins - revised_tm)/60

	report_write.write(str(total_hours) + " hours " + str(revised_tm) + " minutes\n\n")
	indiv_file.close()

# Report completed and closed, then coverted to pdf
report_write.write("\end{document}")
report_write.close()
os.system("pdflatex report.tex")
